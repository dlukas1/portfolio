﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using SportsStore.Models;
using SportsStore.Controllers;

namespace SportsStore.Tests
{
    public class OrderControllerTests
    {
        [Fact]
        public void Can_Check_And_Submit_Order()
        {
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);
            OrderController target = new OrderController(mock.Object, cart);

            //Action - go to checkout
            RedirectToActionResult result =
                target.Checkout(new Order()) as RedirectToActionResult;

            //Assert that order was saved
            mock.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Once);

            //Assert Completed
            Assert.Equal("Completed", result.ActionName);
        }







        [Fact]
        public void Cannot_Checkout_Empty_Cart()
        {
            //Arrange - create fake storage
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();
            //Arrange - create empty cart and controller
            Cart cart = new Cart();
            Order order = new Order();
            OrderController target = new OrderController(mock.Object, cart);

            //Action
            ViewResult result = target.Checkout(order) as ViewResult;

            //Assert that order was not saved
            mock.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Never);

            Assert.True(string.IsNullOrEmpty(result.ViewName));

            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Fact]
        public void Cannot_Checkout_Invalid_ShippingDetails()
        {
            //Arrange
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();
            Cart cart = new Cart();
            cart.AddItem(new Product(), 1);
            //Order order = new Order();
            OrderController target = new OrderController(mock.Object, cart);
            target.ModelState.AddModelError("error", "error");
            //Action - try to checkout
            ViewResult result = target.Checkout(new Order()) as ViewResult;

            //Assert thet order was not saved
            mock.Verify(m => m.SaveOrder(It.IsAny<Order>()), Times.Never);
            //Assert method returns standart view
            Assert.True(string.IsNullOrEmpty(result.ViewName));
            //Assert
            Assert.False(result.ViewData.ModelState.IsValid);
        }
    }
}
