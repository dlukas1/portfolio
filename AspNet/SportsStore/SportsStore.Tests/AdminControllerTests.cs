﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using SportsStore.Models;
using Microsoft.AspNetCore.Mvc;
using SportsStore.Controllers;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace SportsStore.Tests
{
    public class AdminControllerTests
    {
        [Fact]
        public void Can_Delete_Valid_Product()
        {
            //Arrange product
            Product prod = new Product { ProductId = 2, Name = "Test" };
            //Arrange storage
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product{ProductId = 1, Name = "P1"},
                prod,
                new Product{ProductId = 3, Name = "P3"}
            });
            //Arrange controller
            AdminController target = new AdminController(mock.Object);
            //Action - remove prod
            target.Delete(prod.ProductId);
            //Assert
            mock.Verify(m => m.DeleteProduct(prod.ProductId));
        }



        [Fact]
        public void Cannot_Save_Invalid_Changes()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            AdminController target = new AdminController(mock.Object);
            Product product = new Product { Name = "Test" };
            //Arrange - add error to modelstate
            target.ModelState.AddModelError("error", "error");
            //Action - try to save
            IActionResult result = target.Edit(product);

            //Assert was call to storage
            mock.Verify(m => m.SaveProduct(It.IsAny<Product>()), Times.Never);
            Assert.IsType<ViewResult>(result);
        }


        [Fact]
        public void Can_Save_Valid_Changes()
        {
            //Arrange data storage
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            //Arrange temporary data storage
            Mock<ITempDataDictionary> tempData = new Mock<ITempDataDictionary>();
            //Arrange controller
            AdminController target = new AdminController(mock.Object)
            {
                TempData = tempData.Object
            };
            //Arrange product
            Product product = new Product { Name = "Test" };
            //Action - try to save
            IActionResult result = target.Edit(product);
            //Assert that was call to storage
            mock.Verify(m => m.SaveProduct(product));
            //Assert type
            Assert.IsType<RedirectToActionResult>(result);
            Assert.Equal("Index", (result as RedirectToActionResult).ActionName);
        }



        [Fact]
        public void Can_Edit_Product()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product{ProductId = 1, Name = "P1"},
                new Product{ProductId = 2, Name = "P2"},
                new Product{ProductId = 3, Name = "P3"}
            });
            //Arrange controller
            AdminController target = new AdminController(mock.Object);

            //Action
            Product p1 = GetViewModel<Product>(target.Edit(1));
            Product p2 = GetViewModel<Product>(target.Edit(2));
            Product p3 = GetViewModel<Product>(target.Edit(3));
            //Assert
            Assert.Equal(1, p1.ProductId);
            Assert.Equal(2, p2.ProductId);
            Assert.Equal(3, p3.ProductId);
        }

        [Fact]
        public void Cannot_Edit_Nonexist_Product()
        {
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product{ProductId = 1, Name = "P1"},
                new Product{ProductId = 2, Name = "P2"}
            });
            AdminController target = new AdminController(mock.Object);
            Product result = GetViewModel<Product>(target.Edit(3));
            Assert.Null(result);
        }


        [Fact]
        public void Index_Contains_All_Products()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product{ProductId = 1, Name = "P1"},
                new Product{ProductId = 2, Name = "P2"},
                new Product{ProductId = 3, Name = "P3"}
            });
            //Arrange controller
            AdminController target = new AdminController(mock.Object);

            //Action
            Product[] result =
                GetViewModel<IEnumerable<Product>>(target.Index())?.ToArray();
            //Assert
            Assert.Equal(3, result.Length);
            Assert.Equal("P1", result[0].Name);
            Assert.Equal("P2", result[1].Name);
            Assert.Equal("P3", result[2].Name);

        }
        private T GetViewModel<T>(IActionResult result) where T : class
        {
            return (result as ViewResult)?.ViewData.Model as T;
        }
    }
}
