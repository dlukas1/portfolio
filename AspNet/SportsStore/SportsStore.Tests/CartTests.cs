﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SportsStore.Models;
using System.Linq;

namespace SportsStore.Tests
{
    public class CartTests
    {
        [Fact]
        public void Can_Add_Lines()
        {
            //Arrange - create few test goods
            Product p1 = new Product { ProductId = 1, Name = "P1" };
            Product p2 = new Product { ProductId = 2, Name = "P2" };
            //Arrabge - create new Cart
            Cart target = new Cart();
            //Action
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            CartLine[] results = target.Lines.ToArray();
            //Assert
            Assert.Equal(2, results.Length);
            Assert.Equal(p1, results[0].Product);
            Assert.Equal(p2, results[1].Product);
        }
        [Fact]
        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //Arrange - create few test goods
            Product p1 = new Product { ProductId = 1, Name = "P1" };
            Product p2 = new Product { ProductId = 2, Name = "P2" };
            //Arrabge - create new Cart
            Cart target = new Cart();
            //Action
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.AddItem(p1, 10);
            CartLine[] results = target.Lines
                .OrderBy(c => c.Product.ProductId).ToArray();
            //Assert
            Assert.Equal(2, results.Length);
            Assert.Equal(11, results[0].Quanity);
            Assert.Equal(1, results[1].Quanity);
        }
        [Fact]
        public void Can_Remove_Line()
        {
            //Arrange - create few test goods
            Product p1 = new Product { ProductId = 1, Name = "P1" };
            Product p2 = new Product { ProductId = 2, Name = "P2" };
            Product p3 = new Product { ProductId = 3, Name = "P3" };
            //Arrabge - create new Cart
            Cart target = new Cart();
            //Action
            target.AddItem(p1, 1);
            target.AddItem(p2, 3);
            target.AddItem(p3, 5);
            target.AddItem(p2, 1);
            //Action
            target.RemoveLine(p2);
            //Assert
            Assert.Empty(target.Lines.Where(c => c.Product == p2));
            Assert.Equal(2, target.Lines.Count());
        }

        [Fact]
        public void Calculate_Cart_Total()
        {
            //Arrange
            Product p1 = new Product { ProductId = 1, Name = "P1", Price = 10 };
            Product p2 = new Product { ProductId = 2, Name = "P2", Price = 20 };
            Cart target = new Cart();
            //Action
            target.AddItem(p1, 2);
            target.AddItem(p2, 1);
            //Assert
            Assert.Equal(40, target.ComputeTotalValue());
        }

        [Fact]
        public void Can_Clean_Cart()
        {
            //Arrange
            Product p1 = new Product { ProductId = 1, Name = "P1", Price = 10 };
            Product p2 = new Product { ProductId = 2, Name = "P2", Price = 20 };
            Cart target = new Cart();
            //Action
            target.AddItem(p1, 2);
            target.AddItem(p2, 1);
            target.Clear();
            //Assert
            Assert.Empty(target.Lines);
        }
    }
}
