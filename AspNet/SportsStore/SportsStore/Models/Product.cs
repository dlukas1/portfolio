﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        [Required(ErrorMessage ="Please enter product name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter product description")]
        public string Description { get; set; }
        [Required]
        [Range(0.01, double.MaxValue, ErrorMessage ="Please enter positive price")]
        public decimal Price { get; set; }
        [Required(ErrorMessage ="Please specify category")]
        public string Category { get; set; }
    }
}
