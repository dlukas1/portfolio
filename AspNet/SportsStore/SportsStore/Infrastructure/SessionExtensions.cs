﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//SOURCE: benjii.me/2016/07/using-sessions-and-httpcontext-in-aspnetcore-and-mvc-core/
namespace SportsStore.Infrastructure
{
    public static class SessionExtensions
    {
        
            public static void SetObjectAsJson(this ISession session, string key, object value)
            {
                session.SetString(key, JsonConvert.SerializeObject(value));
            }

            public static T GetObjectFromJson<T>(this ISession session, string key)
            {
                var value = session.GetString(key);

                return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
            }
        
    }
}
