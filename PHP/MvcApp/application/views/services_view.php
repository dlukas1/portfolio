<p>
<span class="title">Professional Summary</span><br>
Currently I'm 3rd year student at TTU Estonian IT College. During studying successfully accomplished several IT projects (personal and as a teamlead), demonstrating high level of self-organisation, ability to work in team and writing code in different languages. Also programming became a hobby for me, so besides of college material I always looking forward to improve my scills as IT specialist. 
</p><br>

<p>
	<span class="title">Skills</span><br>
	<ul>
		<li>Good knowledge in C# and Asp.Net platform, Asp.Net Core with Entity Framework, LINQ</li>
		<li>Desktop applications with WPF and Java FX</li>
		<li>Good knowledge in JavaScript (AngularJS, Angular 2+, TypeScript)</li>
		<li>Good knowledge in CSS, Bootstrap, HTML5, AJAX and jQuery</li>
		<li>PHP, Laravel framework</li>
		<li>Good knowledge of MySQL and MongoDB, SQL</li>
	</ul>
</p><br>

<p>
	<span class="title">Work Experience</span><br>
	<ul>
		<li>Captain - Trademarine OU (aprill 2013 - current)</li>
		<li>Service Engineer - Harbour Enterprise (march 2012 - march 2013)</li>
		<li>2nd/Chief officer - M/V Alholmen (aug.2008 - june 2011)</li>
		<li>Ordinary Seaman - Nordic Jet Line (sept.2007 - june 2008)</li>
	</ul>
</p><br>

<p>
	<span class="title">Education</span><br>
	<ul>
		<li>2016 - ...	-	Estonian IT College, Development of IT systems</li>
		<li>2003 - 2008	-	Estonian Maritime Academy, Navigational Officer, bachelor</li>
		<li>1992 - 2003	-	Tallinn Linnamae Gymnasium (ex. 65th school)</li>
	</ul>
</p><br>

<p>
	<span class="title">Hobbies and Interests</span><br>
	<ul>
		<li>Sports - some years ago I was member of junior National Team in epee fencing, currently fencing is still my hobby together with fitness, muay thai, running, cycling and swimming</li>
		<li>Motorcycling - I love to travel with motorcycle, it gives time open mind and feeling alive</li>
		<li>Programming - I'm excited with Arduino projects and Competitive Programming quizzes</li>
	</ul>
</p>