<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Dmitry Lukas</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
		<script src="/js/jquery-1.6.2.js" type="text/javascript"></script>
		<script type="text/javascript">
		// return a random integer between 0 and number
		function random(number) {
			
			return Math.floor( Math.random()*(number+1) );
		};
		
		// show random quote
		$(document).ready(function() { 

			var quotes = $('.quote');
			quotes.hide();
			
			var qlen = quotes.length; //document.write( random(qlen-1) );
			$( '.quote:eq(' + random(qlen-1) + ')' ).show(); //tag:eq(1)
		});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/">Dmitry Lukas</span></a>
				</div>
				<div id="menu">
					<ul>
						<li class="first active"><a href="/">Home</a></li>
						<li><a href="/services">CV</a></li>
						<li><a href="/portfolio">My Projects</a></li>
						<li class="last"><a href="/contacts">Contacts</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">
				<div id="sidebar">
					<div class="side-box">
						<h3>Random quote</h3>
						<p align="justify" class="quote">
						"I have always wished for my computer to be as easy to use as my telephone; my wish has come true because I can no longer figure out how to use my telephone."
							<b>- Bjarne Stroustrup</b>
						</p>
						<p align="justify" class="quote">
						“Computer science education cannot make anybody an expert programmer any more than studying brushes and pigment can make somebody an expert painter.”
							<b>- Eric S. Raymond</b>
						</p>
						<p align="justify" class="quote">
						"Talk is cheap. Show me the code."
							<b>- Linus Torvalds</b>
						</p>
						<p align="justify" class="quote">
						"In theory, theory and practice are the same. In practice, they’re not."
							<b>- Yoggi Berra</b>
						</p>
						<p align="justify" class="quote">
						 "Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program."
							<b>- Linus Torvalds</b>
						</p>
						<p align="justify" class="quote">
						"Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live."
							<b>- Martin Golding</b>
						</p>
					</div>
					<div class="side-box">
						<h3>Main menu</h3>
						<ul class="list">
							<li class="first "><a href="/">Home</a></li>
							<li><a href="/services">CV</a></li>
							<li><a href="/portfolio">My Projects</a></li>
							<li class="last"><a href="/contacts">Contacts</a></li>
						</ul>
					</div>
				</div>
				<div id="content">
					<div class="box">
						<?php include 'application/views/'.$content_view; ?>
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
			<div id="page-bottom">
				<div id="page-bottom-sidebar">
					<h3>My contacts</h3>
					<ul class="list">
						<li class="first">Mobile: (+372) 56 202 350)</li>
						<li>Skype: dmitry.lukas</li>
						<li class="last">email: dmitry.lukas@gmail.com</li>
					</ul>
				</div>
				<div id="page-bottom-content">
					<h3>About me</h3>
					<p>
						I am a beginner developer, with great passion in self-studying, inspired to become a professional
						developer and take part in most challenging projects.
					</p>
				</div>
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">Dmitry Lukas</a> &copy; 2018</a>
		</div>
	</body>
</html>