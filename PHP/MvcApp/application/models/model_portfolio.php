

<?php

class Model_Portfolio extends Model
{
	
	public function get_data()
	{	
		
		return array(
			
			array(
				'Year' => '2016',
				'Technology' => 'Java FX',
				'Description' => 'My first desktop app - Casino roulette',
				'Link' => '<a href="https://github.com/dlukas1/RouletteFXtests2">Roulette</a>'
			),

			array(
				'Year' => '2017',
				'Technology' => 'PHP + MySQL + Javascript',
				'Description' => 'Web site about motorcycles',
				'Link' => '<a href="https://github.com/dlukas1/VR_moto">Motorcycles</a>'
			),

			array(
				'Year' => '2017',
				'Technology' => 'C language, Arduino',
				'Description' => 'Programming Arduino to work as electronic door lock with RFID cards',
				'Link' => '<a href="https://gitlab.com/dmitry.lukas/my-project-i237">Arduino</a>'
			),

			array(
				'Year' => '2018',
				'Technology' => 'Asp.Net WPF with Entity Framework',
				'Description' => 'Desktop app for taxi service',
				'Link' => '<a href="https://bitbucket.org/dlukas1/c-project-taxiservice/src/master/">Taxi</a>'
			),

			array(
				'Year' => '2018',
				'Technology' => 'Javascript, Node.js, AngularJS, Angular 2+, MongoDB',
				'Description' => 'Contact app using different technologies',
				'Link' => '<a href="https://bitbucket.org/dlukas1/portfolio/src/master/JS/">JS Contact App</a>'
			),

			array(
				'Year' => '2018',
				'Technology' => 'Asp.Net Core with EF',
				'Description' => 'Pizza delivery system web site',
				'Link' => 'Work in progress'
			),

		);
	}

}
