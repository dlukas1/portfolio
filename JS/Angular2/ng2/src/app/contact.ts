export class Contact {

    public _id: number;
    public name : string;
    public value : string;
    public selected : boolean = false;

    constructor(name:string, value:string){
        this.name = name;
        this.value = value;
    }
}
