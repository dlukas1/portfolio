import { Component, OnInit } from '@angular/core';
import {Contact} from "../contact";
import {ContactService} from "../contact.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-edit',
    templateUrl:'edit.html',
    styles: []
})
export class EditComponent implements OnInit {

    constructor(private contactService:ContactService,
                private router : Router,
                private activatedRoute : ActivatedRoute) { }

    contact : Contact;
    id: number;
    contactName : string;
    contactValue: string;

    public saveChanges(){
        this.contact.name = this.contactName;
        this.contact.value = this.contactValue;
        this.contactService.updateContact(this.id, this.contact)
            .then(() => this.router.navigateByUrl('/list'));

    }

    ngOnInit() : void {
      this.id = this.activatedRoute.snapshot.params['id'];
      this.contactService.getContact(this.id)
          .then((contact) => {
            this.contact = contact;
            this.contactName = contact.name;
            this.contactValue = contact.value;
            return;
          })
    }

}
