import { Pipe, PipeTransform } from '@angular/core';

//http://www.angulartutorial.net/2017/03/simple-search-using-pipe-in-angular-2.html
//https://www.npmjs.com/package/ng2-search-filter

@Pipe({
    name: 'FilterPipe',
})
export class FilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            input = input.toLowerCase();
            return value.filter(function (el: any) {
                return el.name.toLowerCase().indexOf(input) > -1
                    || el.value.toLowerCase().indexOf(input) > -1;
            })
        }
        return value;
    }
}