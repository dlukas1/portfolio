import { Component, OnInit } from '@angular/core';
import {ContactService} from "../contact.service";
import {Contact} from "../contact";

@Component({
  selector: 'app-list',
  templateUrl: 'list.html',
  styles: []
})
export class ListComponent implements OnInit {

    constructor(private contactService : ContactService){
    }

    searchText : string;

    contacts : Contact[] = [];
    selectedContacts : Contact[];
    ids : number[];
    contCopy = this.contacts;
    term: string = this.searchText;
    newContTitle: string = '';
    newContValue: string = '';

    getData(){
        this.contactService.getContacts()
            .then(contacts => this.contacts = contacts);
    }

    deleteSelected() {
        this.selectedContacts = this.contacts.filter(
            c => c.selected === true
        );
        return this.selectedContacts.map(cont => this.deleteContact(cont._id));
    }

    deleteContact(id: number) : void {
        this.contactService.deleteContact(id)
            .then(() => this.getData());
    }

  ngOnInit() {
      this.getData();
  }
}
