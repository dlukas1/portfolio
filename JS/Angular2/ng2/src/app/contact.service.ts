import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Contact} from "./contact";

@Injectable()
export class ContactService {

  constructor(private http: HttpClient) { }

  public getContacts(): Promise<Contact[]>{
      return this.http.get('/contacts')
          .toPromise()
          .then(result => <Contact[]>result);
  }

  public getContact(id:number) :Promise<Contact>{
    return this.http.get('/contacts/'+id).toPromise()
        .then(result => <Contact> result);
  }

  public insertContact(contact : Contact): Promise<void>{
      return this.http.post('/contacts', contact)
          .toPromise()
          .then(() => <void>null);
  }

  public deleteContact(id: number) :Promise<void>{
      return this.http.delete('/contacts/' + id)
          .toPromise().then(() => <void>null);

  }

  public deleteSelected(ids : number[]){
      /*
      Propably while loop is not best option for deleting
      Currently using map() in list.cpmponent.ts
       */

      var i : number = ids.length;
      while (i>0){
          this.deleteContact(ids[i-1]);
          i--;
      }
      return this.getContacts();
  }

  public updateContact(id:number, contact : Contact) : Promise<void>{
        return this.http.put('contacts/' + id, contact)
            .toPromise().then(() => <void>null);
  }

}
