import { Component, OnInit } from '@angular/core';
import {ContactService} from "../contact.service";
import {Contact} from "../contact";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new',
  templateUrl:'new.html',
  styles: []
})
export class NewComponent implements OnInit {

  public newContTitle:string = '';
  public newContValue:string = '';

    constructor(private contactService : ContactService,
                private router : Router){
    }

    AddNewCont(){
        this.contactService.insertContact(new Contact(this.newContTitle, this.newContValue))
            .then(() =>{
                this.router.navigateByUrl('/');
            });
    }



  ngOnInit() {
  }

}
