'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'contacts';
const url = 'mongodb://tester:1234@ds149138.mlab.com:49138/i399';

class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        console.log("Find all");
        return this.db.collection(COLLECTION).find().toArray();
    }

    insert(data) {
        return this.db.collection(COLLECTION).insertOne(data)
            .catch(error => {
                this.close();
                throw error;
            });
    }

    update(id, data){
        id = new ObjectID(id);
        data._id = id;
        return this.db.collection(COLLECTION)
            .updateOne({_id : id}, data);
    }

    findById(id){
        var id = new ObjectID(id);
        return this.db.collection(COLLECTION).findOne({_id : id})
            .then((data) => {
                return data;
            }).catch(error => {
                console.log(error);
                this.close();
                throw error;
            });
    }

    deleteById(id){
        var id = new ObjectID(id);
        return this.db.collection(COLLECTION)
            .deleteOne({_id : id})
            .catch(error => {
                this.close();
                throw error;
            });
    }

    deleteMany(ids){
        var objectIds = [];
        for (let id of ids) {
            objectIds.push(new ObjectID(id))
        }

        return this.db.collection(COLLECTION)
            .remove({_id: {$in: objectIds}})
    }
    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports=Dao;