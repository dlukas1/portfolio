(function () {
'use strict'
    angular.module('app').controller('addContact', Ctrl);

function Ctrl($http, $location) {

    this.contacts = [];
    this.newName = '';
    this.newPhone = '';
    this.saveNew = saveNew;
    this.back = back;

    function saveNew() {
        /*
        if (this.newName == ''|| this.newPhone ==''){
            alert("Please fill every field!");
            return false;
        }*/
        var nc = {
            name:this.newName,
            phone:this.newPhone,
            selected:false};
        $http.post('api/contacts', nc).then(back);
        //clean input fields
        //this.newName = '';
        //this.newPhone = '';
    }

    function back() {
        $location.path('/list');
    }



}


})();