'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const ObjectID = require('mongodb').ObjectID;
const Dao = require('./dao.js');
const dao = new Dao();
const app = express();
const url = 'mongodb://tester:1234@ds149138.mlab.com:49138/i399';
app.use(bodyParser.json()); // before request handlers
app.use(express.static('./'));
app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.post('/api/contacts', addContact);
app.put('/api/contacts/:id', update);
app.post('/api/contacts/delete', deleteMany);
app.delete('/api/contacts/:id', deleteById);

app.use(errorHandler);

dao.connect(url)
    .then(() => {
        app.listen(3000, () => console.log('Server is running on port 3000'));
    }).catch(err => {
    console.log("MongoDB connection failed: ");
    console.log(err);
    process.exit(1);
});

function getContacts(req, resp) {
    dao.findAll().then(data => resp.json(data));
}

function getContact(req, resp) {
    var id = req.params.id;
    dao.findById(id).
    //then(data => resp.end(JSON.stringify(data)));
    then(data => resp.json(data));
}

function addContact(req, resp) {
    var contact = req.body;
    dao.insert(contact).then(() => resp.end());
}

function update(req, resp){
    var id = req.params.id;
    return dao.update(id, req.body)
        .then(() => resp.end());
}

function deleteById(req, resp) {
    var id = req.params.id;
    dao.deleteById(id)
        .then(() => resp.end());
}

function deleteMany(req, resp){
    var ids = req.body;
    dao.deleteMany(ids)
        .then(data => {resp.end("Deleted");
        });
}

function errorHandler(error, req, resp, next) {
    console.log(error)
    resp.status(500).send('error: ' + error.toString());
}