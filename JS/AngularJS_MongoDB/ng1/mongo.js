'use strict';

const mongodb = require('mongodb');
const Dao = require('dao.js');
const ObjectID = mongodb.ObjectID;

var url = 'mongodb://tester:1234@ds149138.mlab.com:49138/i399';

var data = { name: 'Jack Daniels', phone: '555 666' };

//findAll().then(data => console.log(data));
//findById('5ab3e494dcad3121147b2ac8').then(data => console.log(data));

insert(data);
var dao = new Dao();

dao.connect(url)
    .then(() => {
        return dao.findAll()}).then(data => {
    dao.close();
    console.log(data);
}).catch(error => {
    dao.close();
    console.log('Error: ' + error)
});


function  insert(data) {
    var database;
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        return db.collection('contacts').insertOne(data);
    }).then(() => {
        closeDb(database)
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}


function  findAll() {
    var database;
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        return db.collection('contacts').find().toArray();
    }).then(data => {
        closeDb(database)
        return data;
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}

function  findById(id) {
    var database;

    //ID should be given as OBject, not string!
    id = new ObjectID(id);
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        return db.collection('contacts').findOne({_id: id});
    }).then(data => {
        closeDb(database)
        return data;
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}


function closeDb(database) {
    if (database) {
        database.close();
    }
}
