(function () {
    'use strict';

    angular.module('app').config(RouteConfig);

    function RouteConfig($routeProvider) {

        $routeProvider.when('/search', {
            templateUrl : 'views/list.html',
            controller : 'list',
            controllerAs : 'vm'
        }).when('/search/:id', {
            templateUrl : 'views/details.html',
            controller : 'details',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'views/addContact.html',
            controller : 'addContact',
            controllerAs : 'vm'
        }).when('/edit/:id', {
            templateUrl : 'views/details.html',
            controller : 'details',
            controllerAs : 'vm'
        }).otherwise('/search');//default variant
    }
})();