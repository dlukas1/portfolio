(function  () {
'use strict'
    angular.module('app').controller('list', Ctrl);

function Ctrl($http, modalService, $location) {
    var vm = this;
    this.deleteContact = deleteContact;
    this.addCont = addCont;


    init();

    function init() {
        $http.get('api/contacts').then(function (result) {
            vm.contacts = result.data;
        });
    }

    function addCont() {
        $location.path('/new');
    }

    function deleteContact(id) {
        modalService.confirm().then(function () {
           return $http.delete('api/contacts/' + id).then(init);
        });

    }
}
})();
