(function () {
'use strict';
    
    angular.module('app').controller('details', Ctrl);

function Ctrl($http, $routeParams, $location) {
    var vm = this;
    vm.item = {};
    vm.back = back;
    vm.editContact = editContact;
    var myCont;



    findContact();

    function findContact() {
        $http.get('api/contacts/' + $routeParams.id)
            .then(function (result) {
                vm.item = result.data;
                myCont = result.data;
            })
    }

    function back() {
        $location.path('/list');
    }
    
    function editContact() {
        $http.put('api/contacts/' + myCont._id, myCont).then(back);

    }


}

})();